#include <gtest/gtest.h>
#include "search_sum.h"

class TestSearchTest : public testing::Test
{
    protected:
        int array[10] = {3, 1, 23, 7, 19, 12, 14, 18, 9, 21};
};

TEST_F(TestSearchTest, TestKeyExist)
{
    ASSERT_TRUE(search_sum(array, 10, 26));
}

TEST_F(TestSearchTest, TestKeyNotExist)
{
    ASSERT_FALSE(search_sum(array, 10, 99));
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
