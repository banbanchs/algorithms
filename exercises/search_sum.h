#ifndef SEARCHSUM_H
#define SEARCHSUM_H

#include <stdbool.h>

#if __cplusplus
extern "C" {
#endif

bool search_sum(const int *array, int len, int key);

#if __cplusplus
}
#endif

#endif // SEARCHSUM_H
