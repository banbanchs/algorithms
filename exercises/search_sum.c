/*
 * 2.3-7
 * 描述一个theta(nlgn)算法
 * 给定n个整数的集合S和另一个整数x，该算法能确定S中是否存在两个其和刚好为x的元素
 */

#include <stdio.h>
#include <stdlib.h>
#include "search_sum.h"

static int cmp_int(const void *g, const void *l)
{
    return (*(int *)g > *(int *)l ? 1 : 0);
}

/*
 * First quick sort array ascending
 * then set sum = array[left] + array[right]
 * if sum is less than key, left++, else right--.
 *
 * Time Complexity:
 *  Quicksort is O(nlog(n))
 *  search is O(n)
 *  then *search_sum* is O(nlog(n))
 *
 * @return true is key exists.
 */
bool search_sum(const int *array, int len, int key)
{
    qsort(array, len, sizeof(int), cmp_int);
    int left = 0, right = len-1;
    int sum;
    while (left < right) {
        sum = array[left] + array[right];
        if (sum == key)
            return true;
        else if (sum < key)
            ++left;
        else
            --right;
    }
    return false;
}
