#include <iostream>
#include <gtest/gtest.h>
#include "rand_gen.h"
#include "sort.h"

#define ARRAY_NUM 100

class TestSort : public testing::Test
{
    protected:
        int *array;
        virtual void SetUp() {
            array = new int[ARRAY_NUM];
            rnum_gen(array, ARRAY_NUM);
        }
        virtual void TearDown() {
            delete [] array;
        }
};

TEST_F(TestSort, TestBubbleSort)
{
    bubble_sort(array, ARRAY_NUM);
    for (int i = 0; i < ARRAY_NUM-1; i++)
        ASSERT_GE(array[i+1], array[i]);
}

TEST_F(TestSort, TestInsertionSort)
{
    insertion_sort(array, ARRAY_NUM);
    for (int i = 0; i < ARRAY_NUM-1; i++)
        ASSERT_GE(array[i+1], array[i]);
}

TEST_F(TestSort, TestSelectionSort)
{
    selection_sort(array, ARRAY_NUM);
    for (int i = 0; i < ARRAY_NUM-1; i++)
        ASSERT_GE(array[i+1], array[i]);
}

TEST_F(TestSort, TestMergeSort)
{
    merge_sort(array, 0, ARRAY_NUM-1);
    for (int i = 0; i < ARRAY_NUM-1; i++)
        ASSERT_GE(array[i+1], array[i]);
}

TEST_F(TestSort, TestHeapSort)
{
    heap_sort(array, ARRAY_NUM);
    for (int i = 0; i < ARRAY_NUM-1; i++)
        ASSERT_GE(array[i+1], array[i]);
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
