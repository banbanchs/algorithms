#ifndef RAND_GEN_H
#define RAND_GEN_H

#ifdef __cplusplus
extern "C" {
#endif

void rnum_gen(int *array, int num);

#ifdef __cplusplus
}
#endif

#endif
