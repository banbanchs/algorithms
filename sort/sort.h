#ifndef SORT_H
#define SORT_H

#ifdef __cplusplus
extern "C" {
#endif

void bubble_sort(int *array, int len);
void insertion_sort(int *array, int len);
void selection_sort(int *array, int len);
void merge_sort(int *array, int start, int stop);
void heap_sort(int *array, int len);

#ifdef __cplusplus
}
#endif

#endif
