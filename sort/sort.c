#include <stdio.h>
#include <stdlib.h>
#include "sort.h"

static void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void bubble_sort(int *array, int len)
{
    for (int i = 0; i < len; i++) {
        for (int j = len-1; j >= i; j--) {
            if (array[j] > array[j+1]) {
                int tmp = array[j];
                array[j] = array[j+1];
                array[j+1] = tmp;
            }
        }
    }
}

void insertion_sort(int *array, int len)
{
    for (int j = 1; j < len; j++) {
        int key = array[j];
        // Insert array[j] into the second sequence array[0..j-1]
        int i;
        for (i = j-1; i >= 0 && array[i] > key; i--)
            array[i+1] = array[i];
        array[i+1] = key;
    }
}

void selection_sort(int *array, int len)
{
    for (int i = 0; i < len-1; i++) {
        int min = i;
        for (int j = i+1; j < len; j++) {
            if (array[min] > array[j])
                min = j;
        }
        // swap array[i] and the min of array[i+1:]
        if (min != i) {
            swap(&array[i], &array[min]);
        }
    }
}

/*
 * Merge sort
 * Time complexity: O(nlg)
 */
static void merge(int *array, int p, int q, int r)
{
    int n1 = q - p + 1;     /* length of subarray1 */
    int n2 = r - q;         /* length of subarray2 */
    int left[n1+1];         /* subarray1 */
    int right[n2+1];        /* subarray2 */
    int i, j, k;
    left[n1] = right[n2] = 99999;

    // divide array[p, r] into subarray1[p, q] and subarray[q+1, r]
    for (i = 0; i < n1; i++)
        left[i] = array[p+i];
    for (j = 0; j < n2; j++)
        right[j] = array[q+j+1];

    // find min of two subarray and copy it to array
    i = j = 0;
    for (k = p; k <= r; k++) {
        array[k] = (left[i] < right[j] ? left[i++] : right[j++]);
    }

    // If subarray1/2 is full and subarray2 is not,
    //  copy the reset of subarray2/1 to array.
    while (i == n1 && j < n2)
        array[k++] = right[j++];
    while (j == n2 && i < n1)
        array[k++] = left[i++];
}

void merge_sort(int *array, int start, int end)
{
    if (start < end) {
        int mid = (start + end) / 2;
        merge_sort(array, start, mid);
        merge_sort(array, mid+1, end);
        merge(array, start, mid, end);
    }
}

/*
 * Heap sort
 * Time Complexity: O(nlgn)
 */
#define PARENT(i) (i/2)
#define LEFT(i) (2*i+1)
#define RIGHT(i) (2*i+2)

/*
 * Recover the property of max heap, which means
 * the parent node's value is greater than children nodes' value
 * Time complexity: O(h) (h is the tree height)
 */
static void max_heapify(int *array, int heap_size, int i)
{
    int l = LEFT(i);
    int r = RIGHT(i);
    int largest;
    while (l < heap_size) {
        if (l < heap_size && array[l] > array[i])
            largest = l;
        else
            largest = i;
        if (r < heap_size && array[r] > array[largest])
            largest = r;
        if (largest == i)
            break;
        swap(&array[largest], &array[i]);
        i = largest;
        l = LEFT(i);
        r = RIGHT(i);
    }
}

/*
 * Build heap from the buttom up.
 */
static void build_heap(int *array, int len)
{
    int heap_size = len;
    // loop from A.length/2 downto 1
    // subarray A[(n/2)+1..n] is the leaves of the tree
    // so the loop elements are the leaves' parents.
    for (int i = (len/2); i >= 0; i--)
        max_heapify(array, heap_size, i);
}

void heap_sort(int *array, int len)
{
    build_heap(array, len);
    int heap_size = len;
    for (int i = (len-1); i >= 1; i--) {
        swap(&array[i], &array[0]);
        heap_size--;
        max_heapify(array, heap_size, 0);
    }
}
