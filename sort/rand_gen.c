#include <stdio.h>
#include "rand_gen.h"

void rnum_gen(int *array, int n)
{
    FILE *urandom = fopen("/dev/urandom", "rb");
    int i;
    unsigned short tmp;
    for (i = 0; i < n; ) {
        fread(&tmp, 1, 2, urandom);
        if (tmp == 0)
            continue;
        array[i] = tmp;
        i++;
    }
    fclose(urandom);
}
