#include <stdio.h>

int linear_search(int *array, int start, int end, int key)
{
    int index = -1;
    for (int i = start; i <= end; i++) {
        if (array[i] == key) {
            index = i;
            break;
        }
    }
    return index;
}

int binary_search(int *array, int start, int end, int key)
{
    int mid = start + (end - start) / 2;
    if (start > end)
        return -1;
    if (array[mid] == key)
        return mid;
    else if (array[mid] > key)
        return binary_search(array, start, mid-1, key);
    else
        return binary_search(array, mid+1, end, key);
}
