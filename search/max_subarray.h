#ifndef MAX_SUBARRAY_H
#define MAX_SUBARRAY_H

#ifdef __cplusplus
extern "C" {
#endif

extern int find_maximum_subarray(const int *array, int low, int hight);
extern int find_maximum_subarray_op(const int *array, int low, int hight);
extern int find_maximum_subarray_tr(const int *array, int low, int hight);

#ifdef __cplusplus
}
#endif

#endif // MAX_SUBARRAY_H
