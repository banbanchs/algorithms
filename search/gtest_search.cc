#include <gtest/gtest.h>
#include "search.h"
#include "max_subarray.h"

class TestLinearSearch : public testing::Test
{
    protected:
        int array[6] = {3, 5, 7, 2, 4, 8};
};

class TestBinarySearch : public testing::Test
{
    protected:
        int array[10] = {2, 5, 7, 9, 11, 14, 16, 18, 21, 23};
};

TEST_F(TestLinearSearch, TestKeyExist)
{
    int index = linear_search(array, 0, 5, 2);
    ASSERT_EQ(index, 3);
}

TEST_F(TestLinearSearch, TestKeyNotExist)
{
    int index = linear_search(array, 0, 5, 9);
    ASSERT_EQ(index, -1);
}

TEST_F(TestLinearSearch, TestKeyAtHead)
{
    int index = linear_search(array, 0, 5, 3);
    ASSERT_EQ(index, 0);
}

TEST_F(TestLinearSearch, TestKeyAtTail)
{
    int index = linear_search(array, 0, 5, 8);
    ASSERT_EQ(index, 5);
}

TEST_F(TestBinarySearch, TestKeyExist)
{
    int index = binary_search(array, 0, 9, 16);
    ASSERT_EQ(index, 6);
}

TEST_F(TestBinarySearch, TestKeyNotExist)
{
    int index = binary_search(array, 0, 9, 17);
    ASSERT_EQ(index, -1);
}

TEST_F(TestBinarySearch, TestKeyAtHead)
{
    int index = binary_search(array, 0, 9, 2);
    ASSERT_EQ(index, 0);
}

TEST_F(TestBinarySearch, TestKeyAtTail)
{
    int index = binary_search(array, 0, 9, 23);
    ASSERT_EQ(index, 9);
}

TEST(TestMaxSubarray, TestMaxSubarrayAtCrossing)
{
    int array[16] = {13, -3, -25, 20, -3, -16, -23, 18,
        20, -7, 12, -5, -22, 15, -4, 7};
    int max_subarray_sum = find_maximum_subarray(array, 0, 15);
    int max_subarray_sum_op = find_maximum_subarray_op(array, 0, 15);
    int max_subarray_sum_tr = find_maximum_subarray_tr(array, 0, 15);
    ASSERT_EQ(max_subarray_sum, 43);
    ASSERT_EQ(max_subarray_sum_op, 43);
    ASSERT_EQ(max_subarray_sum_tr, 43);
}

TEST(TestMaxSubarray, TestMaxSubarrayAtLeft)
{
    int array[16] = {13, -3, 20, -3, 15, -25, 8, -16,
        -4, -20, 4, 5, -7, 9, 7, -8};
    int max_subarray_sum = find_maximum_subarray(array, 0, 15);
    int max_subarray_sum_op = find_maximum_subarray_op(array, 0, 15);
    int max_subarray_sum_tr = find_maximum_subarray_tr(array, 0, 15);
    ASSERT_EQ(max_subarray_sum, 42);
    ASSERT_EQ(max_subarray_sum_op, 42);
    ASSERT_EQ(max_subarray_sum_tr, 42);
}

TEST(TestMaxSubarray, TestMaxSubarrayAtRight)
{
    int array[16] = {-3, 4, 2, -9, -13, -8, 4, -9,
        -4, -1, 0, -9, 20, 9, -7, 21};
    int max_subarray_sum = find_maximum_subarray(array, 0, 15);
    int max_subarray_sum_op = find_maximum_subarray_op(array, 0, 15);
    int max_subarray_sum_tr = find_maximum_subarray_tr(array, 0, 15);
    ASSERT_EQ(max_subarray_sum, 43);
    ASSERT_EQ(max_subarray_sum_op, 43);
    ASSERT_EQ(max_subarray_sum_tr, 43);
}

TEST(TestMaxSubarray, TestAllNegative)
{
    int array[10] = {-3, -4, -5, -9, -7, -6, -11, -1, -2, -3};
    int max_subarray_sum = find_maximum_subarray(array, 0, 9);
    int max_subarray_sum_op = find_maximum_subarray_op(array, 0, 9);
    int max_subarray_sum_tr = find_maximum_subarray_tr(array, 0, 9);
    ASSERT_EQ(max_subarray_sum, -1);
    ASSERT_EQ(max_subarray_sum_op, -1);
    ASSERT_EQ(max_subarray_sum_tr, -1);
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
