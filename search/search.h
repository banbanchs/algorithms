#ifndef SEARCH_H
#define SEARCH_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Search whether key in array[start..end].
 *
 * @return index(int) if key exists else -1.
 */
int linear_search(int *array, int start, int end, int key);

/*
 * Search if key in sorted array[start..end].
 *
 * @return index(int) if key exists else -1.
 */
int binary_search(int *array, int start, int end, int key);

#ifdef __cplusplus
}
#endif

#endif
