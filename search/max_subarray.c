/*
 * 寻找一个最大子数组问题（数组中必须含有负数，否则没意义）
 */



/*
 * 分治策略，假设最大子组array[i..j]，数组为array[low..hight]，mid为中点
 * array[i..j]的位置情况必定在以下三种情况以内：
 *   1. 位于子数组array[low..mid]中，即 low <= i <= j <= mid;
 *   2. 位于子数组array[mid+1..hight]中，即 mid+1 <= i <= j <= hight;
 *   3. 跨越了中点mid，即 low <= i <= mid <= j <= hight;
 *
 * 递归查找上面三种情况的最大子组和，并比较
 *
 * 时间复杂度：O(nlgn)
 */

static int find_crossing_subarray(const int *array, int low, int mid, int hight)
{
    // find max sum on left in array[low..mid]
    int left_sum = -0xff;
    int sum = 0;
    for (int i = mid; i >= low; --i) {
        sum += array[i];
        if (sum > left_sum)
            left_sum = sum;
    }

    // find max sum on right in array[mid+1..hight]
    int right_sum = -0xff;
    sum = 0;
    for (int i = mid+1; i <= hight; ++i) {
        sum += array[i];
        if (sum > right_sum)
            right_sum = sum;
    }
    return (left_sum + right_sum);
}

int find_maximum_subarray(const int *array, int low, int hight)
{
    if (low == hight)
        return array[low];
    else {
        int mid = (low + hight) / 2;
        int left_sum = find_maximum_subarray(array, low, mid);
        int right_sum = find_maximum_subarray(array, mid+1, hight);
        int cross_sum = find_crossing_subarray(array, low, mid, hight);
        if (left_sum > right_sum && left_sum > cross_sum)
            return left_sum;
        else if (right_sum > left_sum && right_sum > cross_sum)
            return right_sum;
        else
            return cross_sum;
    }
}

/*
 * 最佳解法
 * 时间复杂度O(n)
 */
int find_maximum_subarray_op(const int *array, int low, int hight)
{
    int max = array[low];
    int sum = 0;
    for (int i = low; i <= hight; ++i) {
        // If sum is non-negative, then add it.
        // Else restart add sum.
        if (sum >= 0)
            sum += array[i];
        else
            sum = array[i];

        // If sum is greater than current max subarray sum, then max = sum
        if (sum > max)
            max = sum;
    }
    return max;
}

/*
 * 传统做法
 * 时间复杂度O(n^2)
 */
int find_maximum_subarray_tr(const int *array, int low, int hight)
{
    int max = -999;
    int sum;
    for (int i = low; i <= hight; ++i) {
        sum = 0;
        for (int j = i; j <= hight; ++j) {
            sum += array[j];
            if (sum > max)
                max = sum;
        }
    }
    return max;
}
